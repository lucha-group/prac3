#include <iostream>
#include <string>
#include <fstream>
#include <vector>

using namespace std;

class Product {
public:
    string Name;
    double Price;
    int Quantity;

    virtual void info() {
        cout << "Name: " << Name << " Price: " << Price << " Quantity: " << Quantity << endl;
    };
};

class Fruits : public Product {
    void info(){
        cout << "Name: " << Name << " Price: " << Price << " Quantity: " << Quantity <<" Cool fruit"<< endl;
    }
};

class Bread : public Product {
    void info(){
        cout << "Name: " << Name << " Price: " << Price << " Quantity: " << Quantity <<" Cool bread"<< endl;
    }
};

class Milk : public Product {
    void info(){
        cout << "Name: " << Name << " Price: " << Price << " Quantity: " << Quantity <<" Cool milk"<< endl;
    }
};

int main() {
    ifstream f("f.txt");
    vector<Product *> squad;
    string a;
    double b;
    int c;

    while (f >> a >> b >> c) {
        if (a == "Fruits") {
            Product *fr = new Fruits();
            fr->Name = a;
            fr->Quantity = c;
            fr->Price = b;
            squad.push_back(fr);
        }
        if (a != "Fruits" && a != "Bread" &&  a != "Milk") {
            Product *pr = new Product();
            pr->Name = a;
            pr->Quantity = c;
            pr->Price = b;
            squad.push_back(pr);
        }
        if (a == "Bread") {
            Product *br = new Bread();
            br->Name = a;
            br->Quantity = c;
            br->Price = b;
            squad.push_back(br);
        }
        if (a == "Milk") {
            Product *ml = new Milk();
            ml->Name = a;
            ml->Quantity = c;
            ml->Price = b;
            squad.push_back(ml);
        }
    }

    for(auto unit : squad)
        unit->info();

    for(auto unit : squad)
        delete unit;
    f.close();
    return 0;
}
